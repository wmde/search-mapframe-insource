defmodule SearchInsourceTest do
  use ExUnit.Case

  test "apply to empty list" do
    SearchInsource.apply_search(
      [],
      fn _ -> flunk() end,
      fn result -> assert result == [] end
    )
  end

  test "apply data flow" do
    site_a = %Wiki.SiteMatrix.Spec{base_url: nil}
    result_a = %{foo: "bar"}

    SearchInsource.apply_search(
      [site_a],
      fn site ->
        assert site == site_a
        result_a
      end,
      fn result ->
        assert result == [result_a]
      end
    )
  end

  test "apply passes error through and continues" do
    site_a = %Wiki.SiteMatrix.Spec{base_url: "1", dbname: "a"}
    site_b = %Wiki.SiteMatrix.Spec{base_url: "2", dbname: "b"}
    error_a = "test reason"
    result_b = %{foo: "bar"}

    SearchInsource.apply_search(
      [site_a, site_b],
      fn site ->
        case site.dbname do
          "a" -> raise error_a

          # TODO: Serialize, wait for first site to raise.
          "b" -> result_b
        end
      end,
      fn result ->
        assert result == [%RuntimeError{message: error_a}, result_b]
      end
    )
  end
end
