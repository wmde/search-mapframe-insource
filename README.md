# SearchInsource

Script to search all Wikimedia sites for `<mapframe>` usages.

## Building

```bash
mix escript.build
```

Run the linters:
```bash
MIX_ENV=test mix credo
```

## Running

```bash
./search_insource <term>
```

When debugging, you may want to limit sites:
```bash
./search_insource --filter enwikivoyage <term>

./search_insource --limit 5 <term>
```

To run the detailed maps report, use the `--maps` flag.
```
./search_insource --filter dewiki --maps
```

To run without building,
```bash
elixir -S mix run -e 'SearchInsource.main([])'
```

## Output

The `out/summary.csv` file gives totals for each wiki.
The `out/details/<wiki>.templates.csv` files give a breakdown of each discovered template and its usages.

Explanation of the columns in `summary.csv`:
* `wiki` - Database name / wiki ID
* `url` - Wiki home page
* `wiki_size` - Total article count
* `active_users` - Number of users active in the past month
* `upper_limit_mapframes` - Naive count of all articles where "mapframe" appears in the rendered version.  This may be higher than the actual number of maps.  We believe this cannot be lower than the real number of maps.
* `proportion_map_pages` - `upper_limit_mapframes / wiki_size`, approximate proportion of all main namespace article pages containing a map.
* `checksum_total_transclusions` - Sum of all transclusions found by searching using individual templates, and directly-entered tags.  This may be missing some pages, and clearly double-counts some pages if multiple map templates are present.
* `direct_mapframes` - How many main namespace articles use `<mapframe>` directly.
* `direct_maplinks` - How many main namespace articles use `<maplink>` directly, not including pages which also have a mapframe.
* `transcluded_mapframe_and_maplink` - Article pages where templates produce both a mapframe and a maplink.
* `transcluded_mapframes` - Article pages where templates render a mapframe but no maplinks.
* `transcluded_maplinks` - Article pages where templates render into a maplink but no mapframe.
* `map_templates` - Total number of templates found, which possibly render a mapframe or maplink.

Explanation of `details/*.templates.csv`:
* `url` - Template page
* `both_rendered` - Count of pages where this template is rendered, containing a mapframe and maplink.
* `mapframe_rendered` - Count of pages where this template is rendered, and the output includes a mapframe but no maplinks.
* `maplink_rendered` - Count of pages where this template is rendered, and the output includes a maplink but no mapframe.
