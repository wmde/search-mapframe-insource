defmodule SearchInsource.MixProject do
  use Mix.Project

  def project do
    [
      app: :search_insource,
      version: "0.1.1",
      elixir: "~> 1.12",
      start_permanent: Mix.env() == :prod,
      deps: deps(),
      escript: [main_module: SearchInsource]
    ]
  end

  # Run "mix help compile.app" to learn about applications.
  def application do
    [
      extra_applications: [:logger]
    ]
  end

  # Run "mix help deps" to learn about dependencies.
  defp deps do
    [
      {:csv, "~> 3.0"},
      {:flow, "~> 1.2"},
      {:retry, "~> 0.17.0"},
      {:mediawiki_client, "~> 0.4"},
      {:credo, "~> 1.0", only: :test, runtime: false}
    ]
  end
end
