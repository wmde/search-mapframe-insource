defmodule MapframeSearchInsource do
  @moduledoc """
  Specialized modules for a detailed report about Kartographer usage.
  """
end

defmodule MapframeSearchInsource.Result do
  @enforce_keys [:site]
  defstruct [
    :site,
    :main_mapframe_count,
    :main_maplink_count,
    :templates,
    :transcluded_mapframe_and_maplink,
    :transcluded_mapframes,
    :transcluded_maplinks,
    :upper_limit_mapframes,
    :approximate_total,
    :wiki_size,
    :active_users,
    :proportion_map_pages,
    :geopoints_by_id,
    :geopoints_by_query
  ]

  @type t :: %__MODULE__{
          site: Wiki.SiteMatrix.Spec.t(),
          main_mapframe_count: number(),
          main_maplink_count: number(),
          templates: list(binary()),
          transcluded_mapframe_and_maplink: number(),
          transcluded_mapframes: number(),
          transcluded_maplinks: number(),
          upper_limit_mapframes: number(),
          approximate_total: number(),
          wiki_size: number(),
          active_users: number(),
          proportion_map_pages: number(),
          geopoints_by_id: number(),
          geopoints_by_query: number()
        }
end

defmodule MapframeSearchInsource.Searcher do
  alias MapframeSearchInsource.Result

  @ns_main 0
  @ns_template 10
  @ns_module 828

  @spec scrape_site(Wiki.SiteMatrix.Spec.t()) :: Result.t()
  def scrape_site(site) do
    %Result{
      site: site
    }
    |> site_stats()
    |> search_articles()
    |> search_possible_mapframes()
    |> search_all_templates()
    |> search_template_usages()
    |> search_geopoints()
  end

  @spec site_stats(Result.t()) :: Result.t()
  def site_stats(out) do
    statistics =
      out.site
      |> Wiki.Action.new()
      |> Wiki.Action.get!(
        action: :query,
        meta: :siteinfo,
        siprop: :statistics
      )
      |> then(fn x -> x.result["query"]["statistics"] end)

    %{
      out
      | wiki_size: Map.get(statistics, "pages"),
        active_users: Map.get(statistics, "activeusers")
    }
  end

  @spec search_articles(Result.t()) :: Result.t()
  defp search_articles(out) do
    %{
      out
      | main_mapframe_count:
          count_article_appearances(
            out.site,
            "insource: mapframe insource:/[<]mapframe/"
          ),
        # TODO: make it clear that this is maplink *only*
        main_maplink_count:
          count_article_appearances(
            out.site,
            "insource: maplink -mapframe insource:/[<]maplink/"
          )
    }
  end

  @spec search_possible_mapframes(Result.t()) :: Result.t()
  defp search_possible_mapframes(out) do
    %{
      out
      | upper_limit_mapframes:
          count_article_appearances(
            out.site,
            "mapframe"
          )
    }
  end

  @spec search_geopoints(Result.t()) :: Result.t()
  defp search_geopoints(out) do
    %{
      out
      | geopoints_by_id:
          count_article_appearances(
            out.site,
            "insource:mapframe insource:/[^|]geopoint[^|]*ids/"
          ),
        geopoints_by_query:
          count_article_appearances(
            out.site,
            "insource:mapframe insource:/[^|]geopoint[^|]*query/"
          )
    }
  end

  @spec count_article_appearances(Wiki.SiteMatrix.Spec.t(), binary()) :: number()
  def count_article_appearances(site, term) do
    site
    |> Wiki.Action.new()
    |> Wiki.Action.get!(
      action: :query,
      list: :search,
      srinfo: :totalhits,
      srnamespace: @ns_main,
      srprop: "",
      srsearch: term
    )
    |> then(fn x -> x.result["query"]["searchinfo"]["totalhits"] end)
  end

  def search_all_templates(out) do
    %{
      out
      | templates:
          (search_templates_for_tag(out.site, "mapframe") ++
             search_templates_for_tag(out.site, "maplink"))
          |> Enum.uniq()
    }
  end

  def search_templates_for_tag(site, tag) do
    site
    |> Wiki.Action.new()
    |> Wiki.Action.get!(
      action: :query,
      list: :search,
      srinfo: "",
      # TODO: either "continue" or report when exceeding the limit.
      srlimit: 500,
      srnamespace: [@ns_template, @ns_module],
      srprop: "",
      srsearch: "insource: #{tag} insource:/[<'\"]#{tag}/"
    )
    |> Map.get(:result)
    |> then(fn result -> result["query"]["search"] end)
    |> Enum.map(fn page -> %{title: page["title"]} end)
  end

  def search_template_usages(out) do
    %{
      out
      | templates:
          out.templates
          |> Flow.from_enumerable()
          |> Flow.partition()
          |> Flow.map(fn template ->
            template
            |> Map.merge(%{
              both_rendered: count_template_usage(out.site, template.title, "mapframe maplink"),
              mapframes_rendered:
                count_template_usage(out.site, template.title, "mapframe -maplink"),
              maplinks_rendered:
                count_template_usage(out.site, template.title, "-mapframe maplink")
            })
          end)
          |> Enum.to_list()
    }
    |> sum_usages()
  end

  def count_template_usage(site, title, filter) do
    site
    |> Wiki.Action.new()
    |> Wiki.Action.get!(
      action: :query,
      list: :search,
      srnamespace: @ns_main,
      srprop: "",
      # Must transclude the template (any depth) but also include mapframe/maplink in the rendered output.
      srsearch: "#{filter} hastemplate: \"#{title}\""
    )
    |> then(fn x -> x.result["query"]["searchinfo"]["totalhits"] end)
  end

  def sum_usages(out) do
    out = %{
      out
      | transcluded_mapframe_and_maplink:
          out.templates |> Enum.map(fn x -> x.both_rendered end) |> Enum.sum(),
        transcluded_mapframes:
          out.templates |> Enum.map(fn x -> x.mapframes_rendered end) |> Enum.sum(),
        transcluded_maplinks:
          out.templates |> Enum.map(fn x -> x.maplinks_rendered end) |> Enum.sum()
    }

    out = %{
      out
      | approximate_total:
          out.main_mapframe_count +
            out.transcluded_mapframe_and_maplink +
            out.transcluded_mapframes +
            out.transcluded_maplinks
    }

    %{
      out
      | proportion_map_pages:
          case out.wiki_size do
            0 -> 0
            wiki_size -> (out.upper_limit_mapframes / wiki_size) |> Float.round(3)
          end
    }
  end
end

defmodule MapframeSearchInsource.Writer do
  defmodule CsvFile do
    def write(data, filename: path, headers: headers) do
      file = File.open!(path, [:write, :utf8])

      data
      |> then(fn x -> [headers] ++ x end)
      |> CSV.encode(delimiter: "\n")
      |> Enum.each(&IO.write(file, &1))

      File.close(file)
    end
  end

  def output(result) do
    File.mkdir_p!("output/details")

    result
    |> Enum.sort(fn x, y ->
      x.proportion_map_pages >= y.proportion_map_pages
    end)
    |> Enum.map(fn x ->
      [
        x.site.dbname,
        x.site.base_url,
        x.wiki_size,
        x.active_users,
        x.upper_limit_mapframes,
        x.proportion_map_pages,
        x.approximate_total,
        x.main_mapframe_count,
        x.main_maplink_count,
        x.transcluded_mapframe_and_maplink,
        x.transcluded_mapframes,
        x.transcluded_maplinks,
        Enum.count(x.templates),
        x.geopoints_by_id,
        x.geopoints_by_query
      ]
    end)
    |> CsvFile.write(
      filename: "output/summary.csv",
      headers: ~W(
        wiki
        url
        wiki_size
        active_users
        upper_limit_mapframes
        proportion_map_pages
        checksum_total_transclusions
        direct_mapframes
        direct_maplinks
        transcluded_mapframe_and_maplink
        transcluded_mapframes
        transcluded_maplinks
        map_templates
        geopoints_by_id
        geopoints_by_query
      )
    )

    result
    |> Enum.reject(fn x -> Enum.empty?(x.templates) end)
    |> Enum.each(fn x ->
      x.templates
      |> Enum.map(fn template ->
        [
          x.site.base_url <> "/wiki/" <> String.replace(template.title, " ", "_"),
          template.both_rendered,
          template.mapframes_rendered,
          template.maplinks_rendered
        ]
      end)
      |> CsvFile.write(
        filename: "output/details/#{x.site.dbname}.templates.csv",
        headers: ~W(url both_rendered mapframe_rendered maplink_rendered)
      )
    end)
  end
end
