# TODO:
#  * call "continue"

defmodule SearchInsource do
  alias SearchInsource.Result
  alias SearchInsource.Searcher
  alias SearchInsource.Writer

  @type scraper :: (Wiki.SiteMatrix.Spec.t() -> any())
  @type writer :: (any() -> none())

  def main(args) do
    {opts, terms, _} =
      OptionParser.parse(args,
        strict: [
          help: :boolean,
          limit: :integer,
          filter: :string,
          maps: :boolean
        ]
      )

    cond do
      opts[:help] ->
        usage()

      opts[:maps] ->
        apply_search(
          get_public_sites(opts),
          &MapframeSearchInsource.Searcher.scrape_site/1,
          &MapframeSearchInsource.Writer.output/1
        )

      length(terms) == 1 ->
        apply_search(
          get_public_sites(opts),
          &Searcher.scrape_site(&1, hd(terms)),
          &Writer.output/1
        )

      true ->
        usage()
    end
  end

  def usage() do
    IO.puts("""
    Usage:
      search_insource [--help] [--limit=<num>] [--filter=<dbname>] <search term>

      search_insource [--help] [--limit=<num>] [--filter=<dbname>] --maps

    Options:
      --limit=<num>      Stop after a limited number of sites, e.g. "5"
      --filter=<dbname>  Only process sites matching filter, e.g. "enwiki"

    Modes:
      --maps             Run a detailed report looking for mapframe and maplink.

    The search term should be enclosed in quotes, and can be as complex as
    required.  Remember to always run a simple insource search before using the
    regex feature.

    Examples:
        search_insource "insource:mapframe -insource:/marker-style=\".*\""
    """)
  end

  @spec apply_search(list(Wiki.SiteMatrix.Spec.t()), scraper(), writer()) :: :ok
  def apply_search(sites, scraper, writer) do
    sites
    |> Flow.from_enumerable()
    |> Flow.partition()
    |> Flow.map(
      fn site ->
        IO.puts("Scraping #{site.dbname}")
        wrap_retry(
          fn -> scraper.(site) end
        )
      end
    )
    |> Enum.to_list()
    |> writer.()
  end

  @spec get_public_sites(keyword()) :: list(Wiki.SiteMatrix.Spec.t())
  defp get_public_sites(opts) do
    {:ok, sites} =
      Wiki.SiteMatrix.new()
      |> Wiki.SiteMatrix.get_all()

    sites
    |> Enum.reject(fn site -> site.closed or site.private end)
    |> filter_by_dbname(opts[:filter])
    |> limit_to_number(opts[:limit])
  end

  defp filter_by_dbname(sites, nil), do: sites

  defp filter_by_dbname(sites, dbname),
    do: Enum.filter(sites, fn site -> site.dbname == dbname end)

  defp limit_to_number(sites, nil), do: sites

  defp limit_to_number(sites, limit), do: Enum.take(sites, limit)

  use Retry

  # Wrap each fn and retry up to 5 times, in case of
  # network or API errors
  @spec wrap_retry((() -> any())) :: any()
  defp wrap_retry(call) do
    retry with: exponential_backoff() |> expiry(300_000) |> Enum.take(5) do
      call.()
    after
      result -> result
    else
      error -> error
    end
  end
end

defmodule SearchInsource.Result do
  @enforce_keys [:site]
  defstruct [
    :site,
    :wiki_size,
    :active_users,
    :total_hits,
    :pages
  ]

  @type t :: %__MODULE__{
          site: Wiki.SiteMatrix.Spec.t(),
          wiki_size: number(),
          active_users: number(),
          total_hits: number(),
          pages: list(binary())
        }
end

defmodule SearchInsource.Searcher do
  alias SearchInsource.Result

  def scrape_site(site, term) do
    %Result{
      site: site
    }
    |> site_stats()
    |> search_for_term(term)
  end

  def site_stats(out) do
    statistics =
      out.site
      |> Wiki.Action.new()
      |> Wiki.Action.get!(
        action: :query,
        meta: :siteinfo,
        siprop: :statistics
      )
      |> then(& &1.result["query"]["statistics"])

    %{
      out
      | wiki_size: Map.get(statistics, "pages"),
        active_users: Map.get(statistics, "activeusers")
    }
  end

  def search_for_term(out, term) do
    result =
      out.site
      |> Wiki.Action.new()
      |> Wiki.Action.get!(
        action: :query,
        list: :search,
        # TODO: either "continue" or report when exceeding the limit.
        srlimit: 500,
        srprop: "",
        srsearch: term
      )
      |> Map.get(:result)

    %{
      out
      | total_hits: result["query"]["searchinfo"]["totalhits"],
        pages:
          result["query"]["search"]
          |> Enum.map(& &1["title"])
    }
  end
end

defmodule SearchInsource.Writer do
  defmodule CsvFile do
    def write(data, filename: path, headers: headers) do
      file = File.open!(path, [:write, :utf8])

      ([headers] ++ data)
      |> CSV.encode(delimiter: "\n")
      |> Enum.each(&IO.write(file, &1))

      File.close(file)
    end
  end

  def output(result) do
    File.mkdir_p!("output/details")

    # TODO: result should have summary/detail data obviously packaged, so the
    # CSVs can be generated dynamically.

    result
    |> Enum.sort(fn x, y ->
      x.total_hits >=
        y.total_hits
        |> case do
          0 -> x.wiki_size >= y.wiki_size
          cmp -> cmp
        end
    end)
    |> Enum.map(fn x ->
      [
        x.site.dbname,
        x.site.base_url,
        x.wiki_size,
        x.active_users,
        x.total_hits
      ]
    end)
    |> CsvFile.write(
      filename: "output/summary.csv",
      headers: ~W(
        wiki
        url
        wiki_size
        active_users
        total_hits
      )
    )

    result
    |> Enum.reject(&Enum.empty?(&1.pages))
    |> Enum.each(fn x ->
      x.pages
      |> Enum.map(fn page ->
        [
          x.site.base_url <> "/wiki/" <> String.replace(page, " ", "_")
        ]
      end)
      |> CsvFile.write(
        filename: "output/details/#{x.site.dbname}.pages.csv",
        headers: ~W(url)
      )
    end)
  end
end
